﻿namespace Shop.UIForms.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Shop.UIForms.Views;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class LoginViewModel : BaseViewModel
    {
        #region Attributs
        private string _email;
        private string _password;
        private bool _isRunning;
        private bool _isEnabled;
        #endregion 

        #region Properties
        public string Email
        {
            get => _email;
            set =>this.SetValue(ref this._email, value);
        }

        public string Password
        {
            get => this._password;
            set => this.SetValue(ref this._password, value);
        }

        public bool IsRunning
        {
            get => this._isRunning;
            set => this.SetValue(ref this._isRunning, value);
        }

        public bool IsEnabled
        {
            get => this._isEnabled;
            set => this.SetValue(ref this._isEnabled, value);
        }
        #endregion 


        #region Commands
        public ICommand LoginCommand { get => new RelayCommand(this.LoginConnnection); }
        #endregion


        #region Methodes
        private async void LoginConnnection()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Error",
                        "You must enter a Email",
                        "Accept");
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Error",
                        "You must enter a Password",
                        "Accept");
                return;
            }

            this.IsRunning = true;
            this.IsEnabled = false;

            await Application.Current.MainPage.DisplayAlert(
                        "OK",
                        "Connection Sucess",
                        "Accept");

            // instancier la page avant de l'afficher
            MainViewModel.GetInstance().ProductsVM = new ProductsViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new ProductsPage());

        }
        #endregion

    }
}
