﻿namespace Shop.UIForms.ViewModels
{
    public class MainViewModel
    {
        private static MainViewModel _instance;
        public LoginViewModel LoginVM { get; set; }
        public ProductsViewModel ProductsVM { get; set; }

        public MainViewModel()
        {
            _instance = this;
        }


        public static MainViewModel GetInstance()
        {
            if (_instance == null)
                return new MainViewModel();

            return _instance;
        }
    }
}
