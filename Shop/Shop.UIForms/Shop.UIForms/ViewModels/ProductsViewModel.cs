﻿namespace Shop.UIForms.ViewModels
{
    using Common.Models;
    using Common.Services;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Xamarin.Forms;

    public class ProductsViewModel : BaseViewModel
    {
        #region Attributs
        private ApiService _apiService;
        private ObservableCollection<Product> _productListProp;

        private bool _isRefreshing;
        #endregion


        #region Properties
        public ObservableCollection<Product> ProductListProp
        {
            get => this._productListProp;
            set => this.SetValue(ref this._productListProp, value);
        }

        public bool IsRefreshing
        {
            get => this._isRefreshing;
            set => SetValue(ref this._isRefreshing, value);
        }
        #endregion


        #region Constuctor
        public ProductsViewModel()
        {
            this._apiService = new ApiService();
            this.LoadProducts();
        }
        #endregion


        #region Methodes
        private async void LoadProducts()
        {
            this.IsRefreshing = true;

            var response = await this._apiService.GetListAsync<Product>(
                "https://shopsiteapp.azurewebsites.net",
                "/api",
                "/Products");

            this.IsRefreshing = false;

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                return;
            }

            var productsApi = (List<Product>)response.Result;
            this.ProductListProp = new ObservableCollection<Product>(productsApi);
        }
        #endregion
    }
}
