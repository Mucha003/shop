﻿namespace Shop.UIForms.Infrastructure
{
    using ViewModels;

    public class InstanceLocator
    {
        public MainViewModel MainVM { get; set; }

        public InstanceLocator()
        {
            MainVM = new MainViewModel();
        }
    }
}
