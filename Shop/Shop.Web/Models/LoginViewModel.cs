﻿namespace Shop.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Username { get; set; }

        [Required]
        [MinLength(6, ErrorMessage = "Must have more than {0}")]
        public string Password { get; set; }

        public bool RememberMe { get; set; }

    }
}
