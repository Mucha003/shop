﻿namespace Shop.Web.Helpers
{
    using Data.Entities;
    using Models;
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public class ProductHelper : IProductHelper
    {
        public Product VMToProduct(ProductViewModel productVM, string src)
        {
            return new Product
            {
                Id = productVM.Id,
                ImageUrl = src,
                IsAvailabe = productVM.IsAvailabe,
                LastPurchase = productVM.LastPurchase,
                LastSale = productVM.LastSale,
                Name = productVM.Name,
                Price = productVM.Price,
                Stock = productVM.Stock,
                User = productVM.User,
                UserId = productVM.UserId
            };
        }


        public ProductViewModel ToProductViewModel(Product product)
        {
            return new ProductViewModel
            {
                Id = product.Id,
                ImageUrl = product.ImageUrl,
                IsAvailabe = product.IsAvailabe,
                LastPurchase = product.LastPurchase,
                LastSale = product.LastSale,
                Name = product.Name,
                Price = product.Price,
                Stock = product.Stock,
                User = product.User,
                UserId = product.UserId
            };
        }


        public async Task<string> SaveImageAsync(ProductViewModel productVM)
        {
            string guid = Guid.NewGuid().ToString();
            string file = $"{guid}.jpg";

            string src = Path.Combine(
                        Directory.GetCurrentDirectory(),
                        "wwwroot\\images\\Products",
                        file);

            using (var stream = new FileStream(src, FileMode.Create))
            {
                await productVM.ImageFile.CopyToAsync(stream);
            }

            // route pour la DB
            return src = $"~/images/Products/{file}";
        }
    }
}
