﻿namespace Shop.Web.Helpers
{
    using Data.Entities;
    using Models;
    using System.Threading.Tasks;

    public interface IProductHelper
    {
        Product VMToProduct(ProductViewModel productVM, string src);
        ProductViewModel ToProductViewModel(Product product);

        Task<string> SaveImageAsync(ProductViewModel productVM);
    }
}
