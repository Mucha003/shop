﻿namespace Shop.Web.Helpers
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Identity;
    using Data.Entities;
    using Models;

    public class UserHelper : IUserHelper
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<User> _signInManager;

        public UserHelper(UserManager<User> pUserManager, 
            SignInManager<User> pSignInManager,
            RoleManager<IdentityRole> pRoleManager)
        {
            this._userManager = pUserManager;
            this._signInManager = pSignInManager;
            this._roleManager = pRoleManager;
        }


        public async Task<IdentityResult> AddUserAsync(User user, string password)
        {
            return await this._userManager.CreateAsync(user, password);
        }


        public async Task<User> GetUserByEmailAsync(string email)
        {
            return await this._userManager.FindByEmailAsync(email);
        }


        public async Task<SignInResult> LoginAsync(LoginViewModel model)
        {
            return await this._signInManager.PasswordSignInAsync(
                                            model.Username,
                                            model.Password,
                                            model.RememberMe,
                                            false);

        }


        public async Task LogoutAsync()
        {
            await this._signInManager.SignOutAsync();
        }


        // IdentityResult pour si ce reussi ou pas.
        public async Task<IdentityResult> UpdateUserAsync(User user)
        {
            return await this._userManager.UpdateAsync(user);
        }


        public async Task<IdentityResult> ChangePasswordAsync(User user, string oldPassword, string newPassword)
        {
            return await this._userManager.ChangePasswordAsync(user, oldPassword, newPassword);

        }


        public async Task<SignInResult> ValidatePasswordAsync(User user, string password)
        {
            return await this._signInManager.CheckPasswordSignInAsync(
                user,
                password,
                false);
        }


        public async Task CheckRoleAsync(string roleName)
        {
            var roleExist = await this._roleManager.RoleExistsAsync(roleName);

            if (!roleExist)
            {
                await this._roleManager.CreateAsync(new IdentityRole { Name = roleName });
            }
        }


        public async Task AddUserToRoleAsync(User user, string roleName)
        {
            await this._userManager.AddToRoleAsync(user, roleName);
        }


        public async Task<bool> IsUserInRoleAsync(User user, string roleName)
        {
            return await this._userManager.IsInRoleAsync(user, roleName);
        }
    }
}
