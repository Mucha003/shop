﻿namespace Shop.Web.Helpers
{
    using Data.Entities;
    using System.Collections.Generic;
    using System.Linq;

    public static class HelpersPanier
    {
        public static decimal GetValueOrderDetailTemp(decimal Price, double Quantity)
        {
            return Price * (decimal)Quantity;
        }

        public static double GetQuantity(List<OrderDetail> items)
        {
            return items == null ? 0 : items.Sum(i => i.Quantity);
        }

        public static decimal GetOrderValues(List<OrderDetail> items)
        {
            return items == null ? 0 : items.Sum(i => i.Value);
        }


    }
}
