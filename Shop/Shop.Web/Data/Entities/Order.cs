﻿namespace Shop.Web.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    public class Order : IEntity
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Order date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd hh:mm tt}", ApplyFormatInEditMode = false)]
        public DateTime OrderDate { get; set; }

        [Display(Name = "Order date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd hh:mm tt}", ApplyFormatInEditMode = false)]
        public DateTime? OrderDateLocal
        {
            get
            {
                if (this.OrderDate == null)
                {
                    return null;
                }

                return this.OrderDate.ToLocalTime();
            }
        }

        [Display(Name = "Delivery date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd hh:mm tt}", ApplyFormatInEditMode = false)]
        public DateTime? DeliveryDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}")]
        public double Quantity { get => this.OrderDetail == null ? 0 : this.OrderDetail.Sum(i => i.Quantity); }

        [DisplayFormat(DataFormatString = "{0:C2}")]
        public decimal Value { get => this.OrderDetail == null ? 0 : this.OrderDetail.Sum(i => i.Value); }

        // .count si ce null il envoi une erreur // correction de la vue ici
        [DisplayFormat(DataFormatString = "{0:N0}")]
        public int Lines { get => this.OrderDetail == null ? 0 : this.OrderDetail.Count(); }


        [Required]
        public virtual User User { get; set; }

        public virtual IEnumerable<OrderDetail> OrderDetail { get; set; }

    }
}
