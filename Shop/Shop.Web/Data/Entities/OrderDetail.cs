﻿namespace Shop.Web.Data.Entities
{
    using System.ComponentModel.DataAnnotations;

    public class OrderDetail : IEntity
    {
        public int Id { get; set; }

        [DisplayFormat(DataFormatString = "{0:C2}")]
        public decimal Price { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}")]
        public double Quantity { get; set; }

        [DisplayFormat(DataFormatString = "{0:C2}")]
        public decimal Value { get { return this.Price * (decimal)this.Quantity; } }


        [Required]
        public virtual Product Product { get; set; }
    }
}
