﻿namespace Shop.Web.Data.Repositories
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Entities;
    using Helpers;
    using Microsoft.EntityFrameworkCore;
    using Shop.Web.Models;

    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        private readonly ShopContext _db;
        private readonly IUserHelper _userHelper;

        public OrderRepository(ShopContext pDb, IUserHelper pUserHelper)
             : base(pDb)
        {
            this._db = pDb;
            this._userHelper = pUserHelper;
        }


        public async Task<IQueryable<Order>> GetOrdersAsync(string userName)
        {
            var user = await this._userHelper.GetUserByEmailAsync(userName);
            if (user == null)
                return null;

            // Tous les orders
            if (await this._userHelper.IsUserInRoleAsync(user, "Admin"))
            {
                return this._db.Orders
                    .Include(o => o.User)
                    .Include(o => o.OrderDetail)
                    .ThenInclude(i => i.Product)
                    .OrderByDescending(o => o.OrderDate);
            }

            // juste ma commande
            return this._db.Orders
                .Include(o => o.OrderDetail)
                .ThenInclude(i => i.Product)
                .Where(o => o.User == user)
                .OrderByDescending(o => o.OrderDate);
        }


        public async Task<IQueryable<OrderDetailTemp>> GetDetailTempAsync(string userName)
        {
            var user = await this._userHelper.GetUserByEmailAsync(userName);
            if (user == null)
                return null;

            return this._db.OrderDetailTemps
                .Include(p => p.Product)
                .Where(u => u.User == user)
                .OrderBy(p => p.Product.Name);
        }


        public async Task AddItemToOrderAsync(AddItemViewModel model, string userName)
        {
            var user = await this._userHelper.GetUserByEmailAsync(userName);
            if (user == null)
                return;


            var product = await this._db.Products.FindAsync(model.ProductId);
            if (product == null)
                return;


            // si ce user a deja pris ce produit il va ajouter jute 1 a sa Quantite
            var orderDetailTemp = await this._db.OrderDetailTemps
                .Where(odt => odt.User == user && odt.Product == product)
                .FirstOrDefaultAsync();

            // si c null il ne la jamais demandé ce produit 
            if (orderDetailTemp == null)
            {
                // donc il va en crer un nouveau
                orderDetailTemp = new OrderDetailTemp
                {
                    Price = product.Price,
                    Product = product,
                    Quantity = model.Quantity,
                    User = user,
                };

                this._db.OrderDetailTemps.Add(orderDetailTemp);
            }
            else
            {
                // il ajoute 1 a quantité.
                orderDetailTemp.Quantity += model.Quantity;
                this._db.OrderDetailTemps.Update(orderDetailTemp);
            }

            await this._db.SaveChangesAsync();
        }


        public async Task ModifyOrderDetailTempQuantityAsync(int id, double quantity)
        {
            var orderDetailTemp = await this._db.OrderDetailTemps.FindAsync(id);
            if (orderDetailTemp == null)
                return;


            orderDetailTemp.Quantity += quantity;
            if (orderDetailTemp.Quantity > 0)
            {
                this._db.OrderDetailTemps.Update(orderDetailTemp);
                await this._db.SaveChangesAsync();
            }
        }


        public async Task DeleteDetailTempAsync(int id)
        {
            var orderDetailTemp = await this._db.OrderDetailTemps.FindAsync(id);
            if (orderDetailTemp == null)
                return;


            this._db.OrderDetailTemps.Remove(orderDetailTemp);
            await this._db.SaveChangesAsync();
        }


        public async Task<bool> ConfirmOrderAsync(string userName)
        {
            var user = await this._userHelper.GetUserByEmailAsync(userName);
            if (user == null)
            {
                return false;
            }

            // envoi moi mom panier
            var orderTmps = await this._db.OrderDetailTemps
                .Include(o => o.Product)
                .Where(o => o.User == user)
                .ToListAsync();

            if (orderTmps == null || orderTmps.Count == 0)
            {
                return false;
            }

            // envoi moi ma commande (select == foreach)
            var details = orderTmps.Select(o => new OrderDetail
            {
                Price = o.Price,
                Product = o.Product,
                Quantity = o.Quantity
            }).ToList();

            var order = new Order
            {
                OrderDate = DateTime.UtcNow, // utc:heure de londres
                User = user,
                OrderDetail = details,
            };

            this._db.Orders.Add(order);
            this._db.OrderDetailTemps.RemoveRange(orderTmps);
            await this._db.SaveChangesAsync();
            return true;
        }


        public async Task DeliverOrder(DeliverViewModel model)
        {
            var order = await this._db.Orders.FindAsync(model.Id);
            if (order == null)
            {
                return;
            }

            order.DeliveryDate = model.DeliveryDate;
            this._db.Orders.Update(order);
            await this._db.SaveChangesAsync();
        }


        public async Task<Order> GetOrdersAsync(int id)
        {
            return await this._db.Orders.FindAsync(id);
        }

    }
}
