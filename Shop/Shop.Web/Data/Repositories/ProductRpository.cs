﻿namespace Shop.Web.Data
{
    using System.Collections.Generic;
    using System.Linq;
    using Entities;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.EntityFrameworkCore;

    public class ProductRpository : GenericRepository<Product>, IProductRepository
    {
        public readonly ShopContext _db;

        public ProductRpository(ShopContext db) : base(db)
        {
            this._db = db;
        }


        public IQueryable GetAllWithUsers()
        {
            return this._db.Products.Include(x => x.User);
        }


        // 3. Creation SelectListItem, 4 ==> Controller(ex: OrdersController)
        public IEnumerable<SelectListItem> GetComboProducts()
        {
            var list = this._db.Products.Select(p => new SelectListItem
            {
                Text = p.Name,
                Value = p.Id.ToString()
            }).ToList();

            list.Insert(0, new SelectListItem
            {
                Text = "(Select a product...)",
                Value = "0"
            });

            return list;
        }

    }
}
