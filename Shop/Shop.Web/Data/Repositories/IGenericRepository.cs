﻿namespace Shop.Web.Data
{
    using System.Linq;
    using System.Threading.Tasks;

    public interface IGenericRepository<T> where T: class
    {
        IQueryable<T> GetAll();
        Task<T> GetByIdAsync(int Id);
        Task CreateAsync(T model);
        Task UpdateAsync(T model);
        Task DeleteAsync(T model);
        Task<bool> ExistAsync(int Id);
    }
}
