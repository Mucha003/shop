﻿namespace Shop.Web.Data
{
    using Entities;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using System.Collections.Generic;
    using System.Linq;

    public interface IProductRepository : IGenericRepository<Product>
    {
        IQueryable GetAllWithUsers();

        // 2. selectList, 3 ==> ProductRepository
        IEnumerable<SelectListItem> GetComboProducts();
    }
}
