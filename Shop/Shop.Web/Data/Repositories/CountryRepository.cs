﻿namespace Shop.Web.Data
{
    using Entities;

    public class CountryRepository: GenericRepository<Country>, ICountryRepository  
    {
        public CountryRepository(ShopContext db) : base(db)
        {
        }
    }
}
