﻿namespace Shop.Web.Data
{
    using Entities;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Threading.Tasks;

    public class GenericRepository<T> : IGenericRepository<T> where T : class, IEntity
    {
        private readonly ShopContext _db; 

        public GenericRepository(ShopContext pDB)
        {
            this._db = pDB;
        }


        public IQueryable<T> GetAll()
        {
            // set<T> == _db.product  => le set va permettre de le rendre generic.
            // AsNoTracking = pour que la methode generique marche.
            return _db.Set<T>().AsNoTracking();
        }


        public async Task<T> GetByIdAsync(int id)
        {
            return await this._db.Set<T>().AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
        }



        public async Task CreateAsync(T model)
        {
            await this._db.Set<T>().AddAsync(model);
            await SaveAllAsync();
        }


        public async Task UpdateAsync(T model)
        {
            this._db.Set<T>().Update(model);
            await SaveAllAsync();
        }


        public async Task DeleteAsync(T model)
        {
            this._db.Set<T>().Remove(model);
            await SaveAllAsync();
        }


        public async Task<bool> ExistAsync(int Id)
        {
            return await this._db.Set<T>().AnyAsync(e => e.Id == Id);
        }


        public async Task<bool> SaveAllAsync()
        {
            return await this._db.SaveChangesAsync() > 0;
        }
    }
}
