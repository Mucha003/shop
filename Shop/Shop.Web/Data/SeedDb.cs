﻿namespace Shop.Web.Data
{
    using Entities;
    using Microsoft.AspNetCore.Identity;
    using Helpers;
    using System;
    using System.Linq;
    using System.Threading.Tasks;


    // Go to Program
    public class SeedDb
    {
        private readonly ShopContext _db;
        private readonly IUserHelper _usesrHelper;

        private Random random;

        public SeedDb(ShopContext pDb, IUserHelper pUserHelper)
        {
            this._db = pDb;
            this._usesrHelper = pUserHelper;
            this.random = new Random();
        }


        public async Task SeedAsync()
        {
            await this._db.Database.EnsureCreatedAsync();

            await this._usesrHelper.CheckRoleAsync("Admin");
            await this._usesrHelper.CheckRoleAsync("Customer");

            var user = await this._usesrHelper.GetUserByEmailAsync("mucha003@gmail.com");

            if (user == null)
            {
                user = new User()
                {
                    FirstName = "Michael",
                    LastName = "Benavides",
                    Email = "mucha003@gmail.com",
                    UserName = "mucha003@gmail.com",
                    PhoneNumber = "0477934871",
                };

                var result = await this._usesrHelper.AddUserAsync(user, "rootroot");

                if (result != IdentityResult.Success)
                {
                    throw new InvalidOperationException("Could not create the user in seeder");
                }

                await this._usesrHelper.AddUserToRoleAsync(user, "Admin");
            }

            var isInRole = await this._usesrHelper.IsUserInRoleAsync(user, "Admin");
            if (!isInRole)
                await this._usesrHelper.AddUserToRoleAsync(user, "Admin");

            if (!this._db.Products.Any())
            {
                this.AddProduct("Iphone X");
                this.AddProduct("Galaxy Tab");
                this.AddProduct("Pc Portable Dell");
                await this._db.SaveChangesAsync();
            }
        }


        private void AddProduct(string name)
        {
            this._db.Products.Add(new Product
            {
                Name = name,
                Price = this.random.Next(1000),
                IsAvailabe = true,
                Stock = this.random.Next(100),
                UserId = 1,
            });
        }

    }
}
