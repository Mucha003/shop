﻿namespace Shop.Web.Controllers.API
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.IdentityModel.Tokens;
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;
    using Helpers;
    using Models;

    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserHelper _userHelper;
        private readonly IConfiguration _configuration;

        public AccountController(IUserHelper pUserHelper, IConfiguration pConfiguration)
        {
            this._userHelper = pUserHelper;
            this._configuration = pConfiguration;
        }


        // Pas oublier d'ajouter a la Startup le token sevice.
        [HttpPost("CreateToken")]
        public async Task<IActionResult> PostCreateToken([FromBody] LoginViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = await this._userHelper.GetUserByEmailAsync(model.Username);

                if (user != null)
                {
                    var result = await this._userHelper.ValidatePasswordAsync(
                        user,
                        model.Password);

                    if (result.Succeeded)
                    {
                        // Liste a Afficher quand le token Arrive
                        var claims = new[]
                        {
                            new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                        };

                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this._configuration["Tokens:Key"]));
                        var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                        // genere le token
                        var token = new JwtSecurityToken(
                            this._configuration["Tokens:Issuer"],
                            this._configuration["Tokens:Audience"],
                            claims,
                            expires: DateTime.UtcNow.AddDays(15),
                            signingCredentials: credentials);
                    
                        // envoi du token
                        var results = new
                        {
                            token = new JwtSecurityTokenHandler().WriteToken(token),
                            expiration = token.ValidTo
                        };

                        return Created(string.Empty, results);
                    }
                }
            }
            // si user n'existe pas
            return BadRequest();
        }

    }
}