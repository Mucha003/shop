﻿namespace Shop.Web.Controllers.API
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Data;

    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class ProductsController : Controller
    {
        private readonly IProductRepository _repos;

        public ProductsController(IProductRepository pRepos)
        {
            this._repos = pRepos;
        }


        // GET: api/Products
        [HttpGet]
        public IActionResult GetProducts()
        {
            return Ok(this._repos.GetAllWithUsers());
        }

    }
}