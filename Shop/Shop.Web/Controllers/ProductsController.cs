﻿namespace Shop.Web.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Data;
    using Data.Entities;
    using Models;
    using Helpers;
    using System.Threading.Tasks;
    using System.Linq;
    using Microsoft.AspNetCore.Authorization;

    public class ProductsController : Controller
    {
        private readonly IProductRepository _repos;
        private readonly IUserHelper _userHelper;
        private readonly IProductHelper _productHelper;

        public ProductsController(IProductRepository pRepos, IUserHelper pUserHelper, IProductHelper pProductHelper)
        {
            this._repos = pRepos;
            this._userHelper = pUserHelper;
            this._productHelper = pProductHelper;
        }


        [HttpGet]
        public IActionResult Index()
        {
            return View(this._repos.GetAll().OrderBy(x => x.Name));
        }


        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundViewResult("ProductNotFound");
            }

            var product = await this._repos.GetByIdAsync(id.Value);

            if (product == null)
            {
                return new NotFoundViewResult("ProductNotFound");
            }

            return View(product);
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProductViewModel productVM)
        {
            if (ModelState.IsValid)
            {
                string src = string.Empty;

                if (productVM.ImageFile != null && productVM.ImageFile.Length > 0)
                {
                    src = await this._productHelper.SaveImageAsync(productVM);
                }

                var user = await this._userHelper.GetUserByEmailAsync(this.User.Identity.Name);
                productVM.User = user;

                Product prod = this._productHelper.VMToProduct(productVM, src);

                await this._repos.CreateAsync(prod);

                return RedirectToAction(nameof(Index));
            }
            return View(productVM);
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new NotFoundViewResult("ProductNotFound");
            }

            var product = await this._repos.GetByIdAsync(id.Value);

            if (product == null)
            {
                return new NotFoundViewResult("ProductNotFound");
            }

            // envoi a la view un VM de product
            var productVM = this._productHelper.ToProductViewModel(product);

            return View(productVM);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ProductViewModel productVM)
        {
            if (id != productVM.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string src = productVM.ImageUrl;

                    if (productVM.ImageFile != null && productVM.ImageFile.Length > 0)
                    {
                        src = await this._productHelper.SaveImageAsync(productVM);
                    }

                    // check User
                    var user = await this._userHelper.GetUserByEmailAsync(this.User.Identity.Name);
                    productVM.User = user;

                    var product = this._productHelper.VMToProduct(productVM, src);

                    await _repos.UpdateAsync(product);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await this._repos.ExistAsync(productVM.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(productVM);
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundViewResult("ProductNotFound");
            }

            var product = await this._repos.GetByIdAsync(id.Value);

            if (product == null)
            {
                return new NotFoundViewResult("ProductNotFound");
            }

            return View(product);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var product = await this._repos.GetByIdAsync(id);
            await this._repos.DeleteAsync(product);

            return RedirectToAction(nameof(Index));
        }


        public IActionResult ProductNotFound()
        {
            return View();
        }
    }
}
